﻿using System;
using System.Windows.Forms;

namespace Lab_1
{
    public partial class MainForm : Form
    {
        private static int INVALID_KEY_VAL = -1;
        private static int N               = 27; // needed to determine the encryption key max values

        public MainForm()
        {
            InitializeComponent();
            _input                         = "";
            btn_set_encryption_key.Enabled = false;
            CheckUndoStatus();

            _encryption_key = new Tuple<int, int>(INVALID_KEY_VAL, INVALID_KEY_VAL);
            _helper         = new AffineCipherHelper(N);

            MessageBox.Show(Utils.ModularMultiplicativeInverse(85, 6864).ToString());
        }

        void CheckUndoStatus()
        {
            btn_backspace.Enabled = _input.Length > 0 ? true : false;
        }

        private void OnBtnPressed(object sender, System.EventArgs e)
        {

            Button btn = sender as Button;
            _input += btn.Text;
            label_input.Text = "Input: " + _input;

            if (!label_output.Equals("Output: "))
                label_output.Text = "Output: ";

            CheckUndoStatus();
            Focus();
        }

        private void OnBackspaceClicked(object sender, System.EventArgs e)
        {
            if (_input.Length > 0)
            {
                _input = _input.Remove(_input.Length - 1);
                label_input.Text = "Input: " + _input;
            }

            CheckUndoStatus();
            label_output.Text = "Output: ";
        }

        private bool HandleEncryptionKey()
        {
            if (!btn_set_encryption_key.Enabled)
            {
                KeyInputForm form = new KeyInputForm();
                form.SetInputMin(0).SetInputMax(N - 1);
                do
                {
                    form.Reset();
                    form.ShowDialog();
                    if (form.OK)
                    {
                        _encryption_key = new Tuple<int, int>(form.X, form.Y);
                        btn_set_encryption_key.Enabled = true;
                        _helper.k = _encryption_key;
                    }

                } while (!AffineCipherHelper.IsKeyValid(_encryption_key.Item1, N) && form.OK);
            }

            return btn_set_encryption_key.Enabled;

        }

        private void OnEncryptClicked(object sender, System.EventArgs e)
        {
            if (!HandleEncryptionKey())
                return;

            label_encryption_key.Text = "Encryption key: (" + _encryption_key.Item1.ToString() + ", " + _encryption_key.Item2.ToString() + ")";

            string encrypted_msg = _helper.EncryptString(_input);

            label_output.Text = "Output: " + encrypted_msg;
            _input = "";
        }

        private void OnDecryptClicked(object sender, System.EventArgs e)
        {
            if (!HandleEncryptionKey())
                return;

            label_encryption_key.Text = "Encryption key: (" + _encryption_key.Item1.ToString() + ", " + _encryption_key.Item2.ToString() + ")";

            string decrypted_msg = _helper.DecryptString(_input);

            label_output.Text = "Output: " + decrypted_msg;
        }

        private void OnResetClicked(object sender, System.EventArgs e)
        {
            btn_set_encryption_key.Enabled = false;
            _encryption_key = new Tuple<int, int>(INVALID_KEY_VAL, INVALID_KEY_VAL);
            label_encryption_key.Text = "Encryption key:";
            label_output.Text = "Output: ";
        }

        private void OnKeyPress(object sender, KeyEventArgs e)
        {
            Button simulated_btn = null;
            switch (e.KeyCode)
            {
                case Keys.Q:
                    simulated_btn = btn_q;
                    break;
                case Keys.W:
                    simulated_btn = btn_w;
                    break;
                case Keys.E:
                    simulated_btn = btn_e;
                    break;
                case Keys.R:
                    simulated_btn = btn_r;
                    break;
                case Keys.T:
                    simulated_btn = btn_t;
                    break;
                case Keys.Y:
                    simulated_btn = btn_y;
                    break;
                case Keys.U:
                    simulated_btn = btn_u;
                    break;
                case Keys.I:
                    simulated_btn = btn_i;
                    break;
                case Keys.O:
                    simulated_btn = btn_o;
                    break;
                case Keys.P:
                    simulated_btn = btn_p;
                    break;
                case Keys.A:
                    simulated_btn = btn_a;
                    break;
                case Keys.S:
                    simulated_btn = btn_s;
                    break;
                case Keys.D:
                    simulated_btn = btn_d;
                    break;
                case Keys.F:
                    simulated_btn = btn_f;
                    break;
                case Keys.G:
                    simulated_btn = btn_g;
                    break;
                case Keys.H:
                    simulated_btn = btn_h;
                    break;
                case Keys.J:
                    simulated_btn = btn_j;
                    break;
                case Keys.K:
                    simulated_btn = btn_k;
                    break;
                case Keys.L:
                    simulated_btn = btn_l;
                    break;
                case Keys.Z:
                    simulated_btn = btn_z;
                    break;
                case Keys.X:
                    simulated_btn = btn_x;
                    break;
                case Keys.C:
                    simulated_btn = btn_c;
                    break;
                case Keys.V:
                    simulated_btn = btn_v;
                    break;
                case Keys.B:
                    simulated_btn = btn_b;
                    break;
                case Keys.N:
                    simulated_btn = btn_n;
                    break;
                case Keys.M:
                    simulated_btn = btn_m;
                    break;
                case Keys.Space:
                    simulated_btn = btn_space;
                    break;
                case Keys.Back:
                    OnBackspaceClicked(null, null);
                    break;
                default:
                    break;
            }

            if (simulated_btn == null)
                return;

            OnBtnPressed(simulated_btn, null);
        }

        private string _input; // holds the sequence of characters currently typed by the user
        private Tuple<int, int> _encryption_key; // holds the encryption key for the affine encryption algorithm
        private AffineCipherHelper _helper;
    }
}

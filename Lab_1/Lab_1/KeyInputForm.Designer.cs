﻿namespace Lab_1
{
    partial class KeyInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_panel = new System.Windows.Forms.TableLayoutPanel();
            this.text_input_panel = new System.Windows.Forms.TableLayoutPanel();
            this.key_y_input = new System.Windows.Forms.NumericUpDown();
            this.key_y_label = new System.Windows.Forms.Label();
            this.key_x_label = new System.Windows.Forms.Label();
            this.key_x_input = new System.Windows.Forms.NumericUpDown();
            this.btn_panel = new System.Windows.Forms.TableLayoutPanel();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_ok = new System.Windows.Forms.Button();
            this.main_panel.SuspendLayout();
            this.text_input_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.key_y_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.key_x_input)).BeginInit();
            this.btn_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_panel
            // 
            this.main_panel.ColumnCount = 1;
            this.main_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_panel.Controls.Add(this.text_input_panel, 0, 0);
            this.main_panel.Controls.Add(this.btn_panel, 0, 1);
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.Location = new System.Drawing.Point(0, 0);
            this.main_panel.Name = "main_panel";
            this.main_panel.RowCount = 2;
            this.main_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.14286F));
            this.main_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.85714F));
            this.main_panel.Size = new System.Drawing.Size(284, 140);
            this.main_panel.TabIndex = 0;
            // 
            // text_input_panel
            // 
            this.text_input_panel.AutoSize = true;
            this.text_input_panel.ColumnCount = 1;
            this.text_input_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.text_input_panel.Controls.Add(this.key_y_input, 0, 3);
            this.text_input_panel.Controls.Add(this.key_y_label, 0, 2);
            this.text_input_panel.Controls.Add(this.key_x_label, 0, 0);
            this.text_input_panel.Controls.Add(this.key_x_input, 0, 1);
            this.text_input_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.text_input_panel.Location = new System.Drawing.Point(3, 3);
            this.text_input_panel.Name = "text_input_panel";
            this.text_input_panel.RowCount = 4;
            this.text_input_panel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.text_input_panel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.text_input_panel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.text_input_panel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.text_input_panel.Size = new System.Drawing.Size(278, 88);
            this.text_input_panel.TabIndex = 0;
            // 
            // key_y_input
            // 
            this.key_y_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.key_y_input.Location = new System.Drawing.Point(3, 55);
            this.key_y_input.Name = "key_y_input";
            this.key_y_input.Size = new System.Drawing.Size(272, 20);
            this.key_y_input.TabIndex = 3;
            this.key_y_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // key_y_label
            // 
            this.key_y_label.AutoSize = true;
            this.key_y_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.key_y_label.Location = new System.Drawing.Point(3, 39);
            this.key_y_label.Name = "key_y_label";
            this.key_y_label.Size = new System.Drawing.Size(272, 13);
            this.key_y_label.TabIndex = 2;
            this.key_y_label.Text = "Y";
            this.key_y_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // key_x_label
            // 
            this.key_x_label.AutoSize = true;
            this.key_x_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.key_x_label.Location = new System.Drawing.Point(3, 0);
            this.key_x_label.Name = "key_x_label";
            this.key_x_label.Size = new System.Drawing.Size(272, 13);
            this.key_x_label.TabIndex = 0;
            this.key_x_label.Text = "X";
            this.key_x_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // key_x_input
            // 
            this.key_x_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.key_x_input.Location = new System.Drawing.Point(3, 16);
            this.key_x_input.Name = "key_x_input";
            this.key_x_input.Size = new System.Drawing.Size(272, 20);
            this.key_x_input.TabIndex = 1;
            this.key_x_input.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_panel
            // 
            this.btn_panel.ColumnCount = 2;
            this.btn_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.btn_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.btn_panel.Controls.Add(this.btn_cancel, 1, 0);
            this.btn_panel.Controls.Add(this.btn_ok, 0, 0);
            this.btn_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_panel.Location = new System.Drawing.Point(3, 97);
            this.btn_panel.Name = "btn_panel";
            this.btn_panel.RowCount = 1;
            this.btn_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.btn_panel.Size = new System.Drawing.Size(278, 40);
            this.btn_panel.TabIndex = 1;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_cancel.Location = new System.Drawing.Point(142, 3);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(133, 34);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.OnCancelClicked);
            // 
            // btn_ok
            // 
            this.btn_ok.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_ok.Location = new System.Drawing.Point(3, 3);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(133, 34);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.OnOKClicked);
            // 
            // KeyInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 140);
            this.Controls.Add(this.main_panel);
            this.Name = "KeyInputForm";
            this.Text = "Enter affine cypher key";
            this.main_panel.ResumeLayout(false);
            this.main_panel.PerformLayout();
            this.text_input_panel.ResumeLayout(false);
            this.text_input_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.key_y_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.key_x_input)).EndInit();
            this.btn_panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_panel;
        private System.Windows.Forms.TableLayoutPanel text_input_panel;
        private System.Windows.Forms.TableLayoutPanel btn_panel;
        private System.Windows.Forms.Label key_x_label;
        private System.Windows.Forms.Label key_y_label;
        private System.Windows.Forms.NumericUpDown key_x_input;
        private System.Windows.Forms.NumericUpDown key_y_input;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_ok;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_1
{
    public partial class KeyInputForm : Form
    {
        public KeyInputForm()
        {
            InitializeComponent();
            _was_ok = false;
        }

        public KeyInputForm SetInputMin(int val)
        {
            key_x_input.Minimum = key_y_input.Minimum = val;

            return this;
        }

        public KeyInputForm SetInputMax(int val)
        {
            key_y_input.Maximum = key_y_input.Maximum = val;

            return this;
        }

        public KeyInputForm Reset()
        {
            _was_ok = false;

            return this;
        }

        public int X
        {
            get
            {
                return _x;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }
        }

        public bool OK
        {
            get
            {
                return _was_ok;
            }
        }

        private void OnOKClicked(object sender, EventArgs e)
        {
            _was_ok = true;

            _x = (int)key_x_input.Value;
            _y = (int)key_y_input.Value;

            Close();
        }

        private int _x;
        private int _y;
        private bool _was_ok;

        private void OnCancelClicked(object sender, EventArgs e)
        {
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Lab_1
{
    public class AffineCipherHelper
    {
        public AffineCipherHelper(int n)
        {
            _n = n;
            InitKeyCodes();
        }

        private void InitKeyCodes()
        {
            _key_codes = new Dictionary<string, int>();
            char letter = 'A';
            for (int i = 1; i < _n; ++i, ++letter)
                _key_codes.Add(letter.ToString(), i);
            _key_codes.Add(" ", 0);
        }

        private int Ek(int x)
        {
            return (_encryption_key.Item1 * x + _encryption_key.Item2) % n;
        }

        private int Dk(int y)
        {
            int a = Utils.ModularMultiplicativeInverse(_encryption_key.Item1, n);
            MessageBox.Show(a.ToString());
            return  a * (y - _encryption_key.Item2) % n;
        }

        private string KeyFromCode(int code)
        {
            return _key_codes.FirstOrDefault(x => x.Value == code).Key;
        }

        public string EncryptString(string msg)
        {
            string encrypted_msg = "";
            string numerical_encryption = "";
            foreach (char letter in msg)
            {
                int ek = Ek(_key_codes[letter.ToString()]);
                numerical_encryption += ek.ToString() + " ";
                encrypted_msg += KeyFromCode(ek);
            }

            //MessageBox.Show("Numerical encryption value: " + numerical_encryption);

            return encrypted_msg;
        }

        public string DecryptString(string msg)
        {
            string decrypted_msg = "";
            string numerical_decryption = "";
            foreach(char letter in msg)
            {
                int dk = Dk(_key_codes[letter.ToString()]);
                numerical_decryption += dk.ToString() + " ";
                decrypted_msg += KeyFromCode(dk);
            }

            //MessageBox.Show("Numerical decryption value: " + numerical_decryption);

            return decrypted_msg;
        }

        public static bool IsKeyValid(int x, int n)
        {
            return Utils.GCD(x, n) == 1;
        }

        public Tuple<int, int> k
        {
            get { return _encryption_key; }
            set { _encryption_key = value; }
        }

        public int n
        {
            get { return _n; }
            set { _n = value; }
        }

        private Tuple<int, int> _encryption_key;
        private int _n;
        private Dictionary<string, int> _key_codes;
    }
}

﻿namespace Lab_1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_layout = new System.Windows.Forms.TableLayoutPanel();
            this.keyboard_panel = new System.Windows.Forms.TableLayoutPanel();
            this.btn_space = new System.Windows.Forms.Button();
            this.btn_p = new System.Windows.Forms.Button();
            this.btn_m = new System.Windows.Forms.Button();
            this.btn_n = new System.Windows.Forms.Button();
            this.btn_b = new System.Windows.Forms.Button();
            this.btn_v = new System.Windows.Forms.Button();
            this.btn_c = new System.Windows.Forms.Button();
            this.btn_x = new System.Windows.Forms.Button();
            this.btn_z = new System.Windows.Forms.Button();
            this.btn_l = new System.Windows.Forms.Button();
            this.btn_k = new System.Windows.Forms.Button();
            this.btn_j = new System.Windows.Forms.Button();
            this.btn_h = new System.Windows.Forms.Button();
            this.btn_g = new System.Windows.Forms.Button();
            this.btn_f = new System.Windows.Forms.Button();
            this.btn_d = new System.Windows.Forms.Button();
            this.btn_s = new System.Windows.Forms.Button();
            this.btn_a = new System.Windows.Forms.Button();
            this.btn_o = new System.Windows.Forms.Button();
            this.btn_i = new System.Windows.Forms.Button();
            this.btn_u = new System.Windows.Forms.Button();
            this.btn_y = new System.Windows.Forms.Button();
            this.btn_t = new System.Windows.Forms.Button();
            this.btn_r = new System.Windows.Forms.Button();
            this.btn_e = new System.Windows.Forms.Button();
            this.btn_w = new System.Windows.Forms.Button();
            this.btn_q = new System.Windows.Forms.Button();
            this.options_panel = new System.Windows.Forms.TableLayoutPanel();
            this.btn_set_encryption_key = new System.Windows.Forms.Button();
            this.btn_encrypt = new System.Windows.Forms.Button();
            this.btn_backspace = new System.Windows.Forms.Button();
            this.btn_decrypt = new System.Windows.Forms.Button();
            this.labels_panel = new System.Windows.Forms.TableLayoutPanel();
            this.label_output = new System.Windows.Forms.Label();
            this.label_input = new System.Windows.Forms.Label();
            this.label_encryption_key = new System.Windows.Forms.Label();
            this.main_layout.SuspendLayout();
            this.keyboard_panel.SuspendLayout();
            this.options_panel.SuspendLayout();
            this.labels_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_layout
            // 
            this.main_layout.ColumnCount = 1;
            this.main_layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_layout.Controls.Add(this.keyboard_panel, 0, 0);
            this.main_layout.Controls.Add(this.options_panel, 0, 1);
            this.main_layout.Controls.Add(this.labels_panel, 0, 2);
            this.main_layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_layout.Location = new System.Drawing.Point(0, 0);
            this.main_layout.Name = "main_layout";
            this.main_layout.RowCount = 3;
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.main_layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.main_layout.Size = new System.Drawing.Size(1078, 441);
            this.main_layout.TabIndex = 0;
            // 
            // keyboard_panel
            // 
            this.keyboard_panel.ColumnCount = 9;
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.keyboard_panel.Controls.Add(this.btn_space, 8, 2);
            this.keyboard_panel.Controls.Add(this.btn_p, 7, 2);
            this.keyboard_panel.Controls.Add(this.btn_m, 6, 2);
            this.keyboard_panel.Controls.Add(this.btn_n, 5, 2);
            this.keyboard_panel.Controls.Add(this.btn_b, 4, 2);
            this.keyboard_panel.Controls.Add(this.btn_v, 3, 2);
            this.keyboard_panel.Controls.Add(this.btn_c, 2, 2);
            this.keyboard_panel.Controls.Add(this.btn_x, 1, 2);
            this.keyboard_panel.Controls.Add(this.btn_z, 0, 2);
            this.keyboard_panel.Controls.Add(this.btn_l, 8, 1);
            this.keyboard_panel.Controls.Add(this.btn_k, 7, 1);
            this.keyboard_panel.Controls.Add(this.btn_j, 6, 1);
            this.keyboard_panel.Controls.Add(this.btn_h, 5, 1);
            this.keyboard_panel.Controls.Add(this.btn_g, 4, 1);
            this.keyboard_panel.Controls.Add(this.btn_f, 3, 1);
            this.keyboard_panel.Controls.Add(this.btn_d, 2, 1);
            this.keyboard_panel.Controls.Add(this.btn_s, 1, 1);
            this.keyboard_panel.Controls.Add(this.btn_a, 0, 1);
            this.keyboard_panel.Controls.Add(this.btn_o, 8, 0);
            this.keyboard_panel.Controls.Add(this.btn_i, 7, 0);
            this.keyboard_panel.Controls.Add(this.btn_u, 6, 0);
            this.keyboard_panel.Controls.Add(this.btn_y, 5, 0);
            this.keyboard_panel.Controls.Add(this.btn_t, 4, 0);
            this.keyboard_panel.Controls.Add(this.btn_r, 3, 0);
            this.keyboard_panel.Controls.Add(this.btn_e, 2, 0);
            this.keyboard_panel.Controls.Add(this.btn_w, 1, 0);
            this.keyboard_panel.Controls.Add(this.btn_q, 0, 0);
            this.keyboard_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyboard_panel.Location = new System.Drawing.Point(3, 3);
            this.keyboard_panel.Name = "keyboard_panel";
            this.keyboard_panel.RowCount = 3;
            this.keyboard_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.keyboard_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.keyboard_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.keyboard_panel.Size = new System.Drawing.Size(1072, 346);
            this.keyboard_panel.TabIndex = 0;
            // 
            // btn_space
            // 
            this.btn_space.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_space.Location = new System.Drawing.Point(955, 233);
            this.btn_space.Name = "btn_space";
            this.btn_space.Size = new System.Drawing.Size(114, 110);
            this.btn_space.TabIndex = 26;
            this.btn_space.Text = " ";
            this.btn_space.UseVisualStyleBackColor = true;
            this.btn_space.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_space.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_p
            // 
            this.btn_p.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_p.Location = new System.Drawing.Point(836, 233);
            this.btn_p.Name = "btn_p";
            this.btn_p.Size = new System.Drawing.Size(113, 110);
            this.btn_p.TabIndex = 25;
            this.btn_p.Text = "P";
            this.btn_p.UseVisualStyleBackColor = true;
            this.btn_p.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_p.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_m
            // 
            this.btn_m.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_m.Location = new System.Drawing.Point(717, 233);
            this.btn_m.Name = "btn_m";
            this.btn_m.Size = new System.Drawing.Size(113, 110);
            this.btn_m.TabIndex = 24;
            this.btn_m.Text = "M";
            this.btn_m.UseVisualStyleBackColor = true;
            this.btn_m.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_m.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_n
            // 
            this.btn_n.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_n.Location = new System.Drawing.Point(598, 233);
            this.btn_n.Name = "btn_n";
            this.btn_n.Size = new System.Drawing.Size(113, 110);
            this.btn_n.TabIndex = 23;
            this.btn_n.Text = "N";
            this.btn_n.UseVisualStyleBackColor = true;
            this.btn_n.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_n.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_b
            // 
            this.btn_b.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_b.Location = new System.Drawing.Point(479, 233);
            this.btn_b.Name = "btn_b";
            this.btn_b.Size = new System.Drawing.Size(113, 110);
            this.btn_b.TabIndex = 22;
            this.btn_b.Text = "B";
            this.btn_b.UseVisualStyleBackColor = true;
            this.btn_b.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_b.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_v
            // 
            this.btn_v.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_v.Location = new System.Drawing.Point(360, 233);
            this.btn_v.Name = "btn_v";
            this.btn_v.Size = new System.Drawing.Size(113, 110);
            this.btn_v.TabIndex = 21;
            this.btn_v.Text = "V";
            this.btn_v.UseVisualStyleBackColor = true;
            this.btn_v.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_v.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_c
            // 
            this.btn_c.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_c.Location = new System.Drawing.Point(241, 233);
            this.btn_c.Name = "btn_c";
            this.btn_c.Size = new System.Drawing.Size(113, 110);
            this.btn_c.TabIndex = 20;
            this.btn_c.Text = "C";
            this.btn_c.UseVisualStyleBackColor = true;
            this.btn_c.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_c.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_x
            // 
            this.btn_x.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_x.Location = new System.Drawing.Point(122, 233);
            this.btn_x.Name = "btn_x";
            this.btn_x.Size = new System.Drawing.Size(113, 110);
            this.btn_x.TabIndex = 19;
            this.btn_x.Text = "X";
            this.btn_x.UseVisualStyleBackColor = true;
            this.btn_x.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_x.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_z
            // 
            this.btn_z.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_z.Location = new System.Drawing.Point(3, 233);
            this.btn_z.Name = "btn_z";
            this.btn_z.Size = new System.Drawing.Size(113, 110);
            this.btn_z.TabIndex = 18;
            this.btn_z.Text = "Z";
            this.btn_z.UseVisualStyleBackColor = true;
            this.btn_z.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_z.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_l
            // 
            this.btn_l.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_l.Location = new System.Drawing.Point(955, 118);
            this.btn_l.Name = "btn_l";
            this.btn_l.Size = new System.Drawing.Size(114, 109);
            this.btn_l.TabIndex = 17;
            this.btn_l.Text = "L";
            this.btn_l.UseVisualStyleBackColor = true;
            this.btn_l.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_l.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_k
            // 
            this.btn_k.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_k.Location = new System.Drawing.Point(836, 118);
            this.btn_k.Name = "btn_k";
            this.btn_k.Size = new System.Drawing.Size(113, 109);
            this.btn_k.TabIndex = 16;
            this.btn_k.Text = "K";
            this.btn_k.UseVisualStyleBackColor = true;
            this.btn_k.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_k.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_j
            // 
            this.btn_j.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_j.Location = new System.Drawing.Point(717, 118);
            this.btn_j.Name = "btn_j";
            this.btn_j.Size = new System.Drawing.Size(113, 109);
            this.btn_j.TabIndex = 15;
            this.btn_j.Text = "J";
            this.btn_j.UseVisualStyleBackColor = true;
            this.btn_j.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_j.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_h
            // 
            this.btn_h.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_h.Location = new System.Drawing.Point(598, 118);
            this.btn_h.Name = "btn_h";
            this.btn_h.Size = new System.Drawing.Size(113, 109);
            this.btn_h.TabIndex = 14;
            this.btn_h.Text = "H";
            this.btn_h.UseVisualStyleBackColor = true;
            this.btn_h.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_h.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_g
            // 
            this.btn_g.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_g.Location = new System.Drawing.Point(479, 118);
            this.btn_g.Name = "btn_g";
            this.btn_g.Size = new System.Drawing.Size(113, 109);
            this.btn_g.TabIndex = 13;
            this.btn_g.Text = "G";
            this.btn_g.UseVisualStyleBackColor = true;
            this.btn_g.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_g.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_f
            // 
            this.btn_f.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_f.Location = new System.Drawing.Point(360, 118);
            this.btn_f.Name = "btn_f";
            this.btn_f.Size = new System.Drawing.Size(113, 109);
            this.btn_f.TabIndex = 12;
            this.btn_f.Text = "F";
            this.btn_f.UseVisualStyleBackColor = true;
            this.btn_f.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_f.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_d
            // 
            this.btn_d.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_d.Location = new System.Drawing.Point(241, 118);
            this.btn_d.Name = "btn_d";
            this.btn_d.Size = new System.Drawing.Size(113, 109);
            this.btn_d.TabIndex = 11;
            this.btn_d.Text = "D";
            this.btn_d.UseVisualStyleBackColor = true;
            this.btn_d.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_d.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_s
            // 
            this.btn_s.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_s.Location = new System.Drawing.Point(122, 118);
            this.btn_s.Name = "btn_s";
            this.btn_s.Size = new System.Drawing.Size(113, 109);
            this.btn_s.TabIndex = 10;
            this.btn_s.Text = "S";
            this.btn_s.UseVisualStyleBackColor = true;
            this.btn_s.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_s.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_a
            // 
            this.btn_a.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_a.Location = new System.Drawing.Point(3, 118);
            this.btn_a.Name = "btn_a";
            this.btn_a.Size = new System.Drawing.Size(113, 109);
            this.btn_a.TabIndex = 9;
            this.btn_a.Text = "A";
            this.btn_a.UseVisualStyleBackColor = true;
            this.btn_a.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_o
            // 
            this.btn_o.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_o.Location = new System.Drawing.Point(955, 3);
            this.btn_o.Name = "btn_o";
            this.btn_o.Size = new System.Drawing.Size(114, 109);
            this.btn_o.TabIndex = 8;
            this.btn_o.Text = "O";
            this.btn_o.UseVisualStyleBackColor = true;
            this.btn_o.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_o.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_i
            // 
            this.btn_i.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_i.Location = new System.Drawing.Point(836, 3);
            this.btn_i.Name = "btn_i";
            this.btn_i.Size = new System.Drawing.Size(113, 109);
            this.btn_i.TabIndex = 7;
            this.btn_i.Text = "I";
            this.btn_i.UseVisualStyleBackColor = true;
            this.btn_i.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_i.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_u
            // 
            this.btn_u.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_u.Location = new System.Drawing.Point(717, 3);
            this.btn_u.Name = "btn_u";
            this.btn_u.Size = new System.Drawing.Size(113, 109);
            this.btn_u.TabIndex = 6;
            this.btn_u.Text = "U";
            this.btn_u.UseVisualStyleBackColor = true;
            this.btn_u.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_u.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_y
            // 
            this.btn_y.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_y.Location = new System.Drawing.Point(598, 3);
            this.btn_y.Name = "btn_y";
            this.btn_y.Size = new System.Drawing.Size(113, 109);
            this.btn_y.TabIndex = 5;
            this.btn_y.Text = "Y";
            this.btn_y.UseVisualStyleBackColor = true;
            this.btn_y.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_y.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_t
            // 
            this.btn_t.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_t.Location = new System.Drawing.Point(479, 3);
            this.btn_t.Name = "btn_t";
            this.btn_t.Size = new System.Drawing.Size(113, 109);
            this.btn_t.TabIndex = 4;
            this.btn_t.Text = "T";
            this.btn_t.UseVisualStyleBackColor = true;
            this.btn_t.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_t.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_r
            // 
            this.btn_r.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_r.Location = new System.Drawing.Point(360, 3);
            this.btn_r.Name = "btn_r";
            this.btn_r.Size = new System.Drawing.Size(113, 109);
            this.btn_r.TabIndex = 3;
            this.btn_r.Text = "R";
            this.btn_r.UseVisualStyleBackColor = true;
            this.btn_r.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_r.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_e
            // 
            this.btn_e.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_e.Location = new System.Drawing.Point(241, 3);
            this.btn_e.Name = "btn_e";
            this.btn_e.Size = new System.Drawing.Size(113, 109);
            this.btn_e.TabIndex = 2;
            this.btn_e.Text = "E";
            this.btn_e.UseVisualStyleBackColor = true;
            this.btn_e.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_e.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_w
            // 
            this.btn_w.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_w.Location = new System.Drawing.Point(122, 3);
            this.btn_w.Name = "btn_w";
            this.btn_w.Size = new System.Drawing.Size(113, 109);
            this.btn_w.TabIndex = 1;
            this.btn_w.Text = "W";
            this.btn_w.UseVisualStyleBackColor = true;
            this.btn_w.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_w.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_q
            // 
            this.btn_q.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_q.Location = new System.Drawing.Point(3, 3);
            this.btn_q.Name = "btn_q";
            this.btn_q.Size = new System.Drawing.Size(113, 109);
            this.btn_q.TabIndex = 0;
            this.btn_q.Text = "Q";
            this.btn_q.UseVisualStyleBackColor = true;
            this.btn_q.Click += new System.EventHandler(this.OnBtnPressed);
            this.btn_q.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // options_panel
            // 
            this.options_panel.ColumnCount = 4;
            this.options_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.options_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.options_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.options_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.options_panel.Controls.Add(this.btn_set_encryption_key, 3, 0);
            this.options_panel.Controls.Add(this.btn_encrypt, 1, 0);
            this.options_panel.Controls.Add(this.btn_backspace, 0, 0);
            this.options_panel.Controls.Add(this.btn_decrypt, 2, 0);
            this.options_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.options_panel.Location = new System.Drawing.Point(3, 355);
            this.options_panel.Name = "options_panel";
            this.options_panel.RowCount = 1;
            this.options_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.options_panel.Size = new System.Drawing.Size(1072, 38);
            this.options_panel.TabIndex = 1;
            // 
            // btn_set_encryption_key
            // 
            this.btn_set_encryption_key.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_set_encryption_key.Location = new System.Drawing.Point(807, 3);
            this.btn_set_encryption_key.Name = "btn_set_encryption_key";
            this.btn_set_encryption_key.Size = new System.Drawing.Size(262, 32);
            this.btn_set_encryption_key.TabIndex = 6;
            this.btn_set_encryption_key.Text = "Reset encryption key";
            this.btn_set_encryption_key.UseVisualStyleBackColor = true;
            this.btn_set_encryption_key.Click += new System.EventHandler(this.OnResetClicked);
            this.btn_set_encryption_key.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_encrypt
            // 
            this.btn_encrypt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_encrypt.Location = new System.Drawing.Point(271, 3);
            this.btn_encrypt.Name = "btn_encrypt";
            this.btn_encrypt.Size = new System.Drawing.Size(262, 32);
            this.btn_encrypt.TabIndex = 0;
            this.btn_encrypt.Text = "Encrypt message";
            this.btn_encrypt.UseVisualStyleBackColor = true;
            this.btn_encrypt.Click += new System.EventHandler(this.OnEncryptClicked);
            this.btn_encrypt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_backspace
            // 
            this.btn_backspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_backspace.Location = new System.Drawing.Point(3, 3);
            this.btn_backspace.Name = "btn_backspace";
            this.btn_backspace.Size = new System.Drawing.Size(262, 32);
            this.btn_backspace.TabIndex = 4;
            this.btn_backspace.Text = "Undo";
            this.btn_backspace.UseVisualStyleBackColor = true;
            this.btn_backspace.Click += new System.EventHandler(this.OnBackspaceClicked);
            this.btn_backspace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // btn_decrypt
            // 
            this.btn_decrypt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_decrypt.Location = new System.Drawing.Point(539, 3);
            this.btn_decrypt.Name = "btn_decrypt";
            this.btn_decrypt.Size = new System.Drawing.Size(262, 32);
            this.btn_decrypt.TabIndex = 5;
            this.btn_decrypt.Text = "Decrypt message";
            this.btn_decrypt.UseVisualStyleBackColor = true;
            this.btn_decrypt.Click += new System.EventHandler(this.OnDecryptClicked);
            this.btn_decrypt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // labels_panel
            // 
            this.labels_panel.ColumnCount = 3;
            this.labels_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.labels_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.labels_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.labels_panel.Controls.Add(this.label_output, 1, 0);
            this.labels_panel.Controls.Add(this.label_input, 0, 0);
            this.labels_panel.Controls.Add(this.label_encryption_key, 2, 0);
            this.labels_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labels_panel.Location = new System.Drawing.Point(3, 399);
            this.labels_panel.Name = "labels_panel";
            this.labels_panel.RowCount = 1;
            this.labels_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.labels_panel.Size = new System.Drawing.Size(1072, 39);
            this.labels_panel.TabIndex = 2;
            // 
            // label_output
            // 
            this.label_output.AutoSize = true;
            this.label_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_output.Location = new System.Drawing.Point(360, 0);
            this.label_output.Name = "label_output";
            this.label_output.Size = new System.Drawing.Size(351, 39);
            this.label_output.TabIndex = 1;
            this.label_output.Text = "Output: ";
            this.label_output.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_input
            // 
            this.label_input.AutoSize = true;
            this.label_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_input.Location = new System.Drawing.Point(3, 0);
            this.label_input.Name = "label_input";
            this.label_input.Size = new System.Drawing.Size(351, 39);
            this.label_input.TabIndex = 0;
            this.label_input.Text = "Input: ";
            this.label_input.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_encryption_key
            // 
            this.label_encryption_key.AutoSize = true;
            this.label_encryption_key.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_encryption_key.Location = new System.Drawing.Point(717, 0);
            this.label_encryption_key.Name = "label_encryption_key";
            this.label_encryption_key.Size = new System.Drawing.Size(352, 39);
            this.label_encryption_key.TabIndex = 2;
            this.label_encryption_key.Text = "Encryption key:";
            this.label_encryption_key.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 441);
            this.Controls.Add(this.main_layout);
            this.Name = "MainForm";
            this.Text = "Affine encrypter";
            this.Click += new System.EventHandler(this.OnBtnPressed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            this.main_layout.ResumeLayout(false);
            this.keyboard_panel.ResumeLayout(false);
            this.options_panel.ResumeLayout(false);
            this.labels_panel.ResumeLayout(false);
            this.labels_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_layout;
        private System.Windows.Forms.TableLayoutPanel keyboard_panel;
        private System.Windows.Forms.Button btn_space;
        private System.Windows.Forms.Button btn_p;
        private System.Windows.Forms.Button btn_m;
        private System.Windows.Forms.Button btn_n;
        private System.Windows.Forms.Button btn_b;
        private System.Windows.Forms.Button btn_v;
        private System.Windows.Forms.Button btn_c;
        private System.Windows.Forms.Button btn_x;
        private System.Windows.Forms.Button btn_z;
        private System.Windows.Forms.Button btn_l;
        private System.Windows.Forms.Button btn_k;
        private System.Windows.Forms.Button btn_j;
        private System.Windows.Forms.Button btn_h;
        private System.Windows.Forms.Button btn_g;
        private System.Windows.Forms.Button btn_f;
        private System.Windows.Forms.Button btn_d;
        private System.Windows.Forms.Button btn_s;
        private System.Windows.Forms.Button btn_a;
        private System.Windows.Forms.Button btn_o;
        private System.Windows.Forms.Button btn_i;
        private System.Windows.Forms.Button btn_u;
        private System.Windows.Forms.Button btn_y;
        private System.Windows.Forms.Button btn_t;
        private System.Windows.Forms.Button btn_r;
        private System.Windows.Forms.Button btn_e;
        private System.Windows.Forms.Button btn_w;
        private System.Windows.Forms.Button btn_q;
        private System.Windows.Forms.TableLayoutPanel options_panel;
        private System.Windows.Forms.TableLayoutPanel labels_panel;
        private System.Windows.Forms.Button btn_encrypt;
        private System.Windows.Forms.Label label_input;
        private System.Windows.Forms.Label label_output;
        private System.Windows.Forms.Button btn_backspace;
        private System.Windows.Forms.Button btn_set_encryption_key;
        private System.Windows.Forms.Button btn_decrypt;
        private System.Windows.Forms.Label label_encryption_key;
    }
}


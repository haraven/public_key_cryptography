﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1
{
    class Utils
    {
        public static int GCD(int a, int b)
        {
            return b == 0 ? a : GCD(b, a % b);
        }

        // Outputs a tuple of the form <u, v, d>, where u and v are integers, and a * u + b * v = d
        private static Tuple<int, int, int> ExtendedEuclid(int a, int b)
        {
            int u1 = 0, u2 = 1, v1 = 1, v2 = 0, q, r, u, v;
            while (b > 0)
            {
                q = a / b;
                r = a - q * b;
                u = u2 - q * u1;
                v = v2 - q * v1;
                a = b;
                b = r;
                u2 = u1;
                u1 = u;
                v2 = v1;
                v1 = v;
            }

            return new Tuple<int, int, int>(u1, v1, a);
        }

        public static int ModularMultiplicativeInverse(int a, int b)
        {
            return ExtendedEuclid(a, b).Item1;
        }
    }
}

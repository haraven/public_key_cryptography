#pragma once
#include <string>
#include <vector>

namespace hrv
{
    namespace pkc
    {
        class Congruence
        {
        public:
            Congruence();
            Congruence(const std::string& unknown, const int32_t& rhs, const int32_t& mod);
            Congruence(const int32_t& lhs, const std::string& unknown, const int32_t& rhs, const int32_t& mod);
            
            hrv::pkc::Congruence& SetUnknown(const std::string& unknown);
            hrv::pkc::Congruence& SetLHS(const int32_t& lhs);
            hrv::pkc::Congruence& SetRHS(const int32_t& rhs);
            hrv::pkc::Congruence& SetMod(const int32_t& mod);
            
            const std::string& GetUnknown() const;
            const int32_t& GetLHS() const;
            const int32_t& GetRHS() const;
            const int32_t& GetMod() const;

            std::string ToString() const;

            ~Congruence();
        private:
            std::string _unknown_term;
            int32_t _lhs;
            int32_t _rhs;
            int32_t _mod;
        };

        class CongruenceSystem
        {
        public:
            CongruenceSystem() {}

            hrv::pkc::CongruenceSystem& Add(Congruence* congruence);
            template<class InputIterator>
            hrv::pkc::CongruenceSystem& Add(InputIterator first, InputIterator last);

            bool HasSolution() const;

            Congruence Solve() const;

            std::string ToString() const;

            ~CongruenceSystem();
        private:
            std::vector<Congruence*> _system;
        };

        template<class InputIterator>
        inline hrv::pkc::CongruenceSystem & CongruenceSystem::Add(InputIterator first, InputIterator last)
        {
            while (first != last)
                _system.push_back(*first++);

            return *this;
        }
    }
}

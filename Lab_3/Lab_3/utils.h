#pragma once

namespace hrv
{
    namespace utils
    {
        template<typename T>
        void SafeDelete(T* a)
        {
            if (a)
            {
                delete a;
                a = nullptr;
            }
        }

        template<typename T>
        void SafeDeleteArr(T* a)
        {
            if (a)
            {
                delete[] a;
                a = nullptr;
            }
        }
    }
}
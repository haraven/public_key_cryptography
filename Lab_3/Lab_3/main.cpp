
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP

#include <iostream>
//#include "math_helper.h"
#include "congruence.h"

using namespace hrv::pkc;
enum RetCode
{
    SUCCESS = 0,
    FAILURE = -1
};

int main()
{
    //std::cout << hrv::math::MathHelper::ModularMultiplicativeInverse(85, 6864);

    CongruenceSystem system;
    system.Add(new Congruence
    (
        "x",
        1,
        5
    )).Add(new Congruence
    (
        "x",
        2,
        7
    )).Add(new Congruence
    (
        "x",
        3,
        9
    )).Add(new Congruence
    (
        "x",
        4,
        11
    ));

    LOG(INFO) << std::endl << system.ToString();
    if (system.HasSolution())
        LOG(INFO) << "The system has a solution, namely " << system.Solve().ToString();
    else
        LOG(INFO) << "The system has no solution";

    return SUCCESS;
}
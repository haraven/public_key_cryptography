﻿#include "congruence.h"
#include <math_helper.h>
#include <logger.hpp>
#include "utils.h"

hrv::pkc::Congruence::Congruence()
: _lhs(INT32_MAX)
, _unknown_term("")
{}

hrv::pkc::Congruence::Congruence(const std::string & unknown, const int32_t & rhs, const int32_t & mod)
: _unknown_term(unknown)
, _lhs(INT32_MAX)
, _rhs(rhs)
, _mod(mod)
{}

hrv::pkc::Congruence::Congruence(const int32_t & lhs, const std::string & unknown, const int32_t & rhs, const int32_t & mod)
: Congruence(unknown, rhs, mod)
{
    _lhs = lhs;
}

hrv::pkc::Congruence & hrv::pkc::Congruence::SetUnknown(const std::string & unknown)
{
    _unknown_term = unknown;

    return *this;
}

hrv::pkc::Congruence & hrv::pkc::Congruence::SetLHS(const int32_t & lhs)
{
    _lhs = lhs;

    return *this;
}

hrv::pkc::Congruence & hrv::pkc::Congruence::SetRHS(const int32_t & rhs)
{
    _rhs = rhs;

    return *this;
}

hrv::pkc::Congruence & hrv::pkc::Congruence::SetMod(const int32_t & mod)
{
    _mod = mod;

    return *this;
}

const std::string & hrv::pkc::Congruence::GetUnknown() const
{
    return _unknown_term;
}

const int32_t & hrv::pkc::Congruence::GetLHS() const
{
    return _lhs;
}

const int32_t & hrv::pkc::Congruence::GetRHS() const
{
    return _rhs;
}

const int32_t & hrv::pkc::Congruence::GetMod() const
{
    return _mod;
}

std::string hrv::pkc::Congruence::ToString() const
{
    return (_lhs == INT32_MAX ? "" : std::to_string(_lhs)) + _unknown_term + " = " + std::to_string(_rhs) + " (mod " + std::to_string(_mod) + ")";
}

hrv::pkc::Congruence::~Congruence()
{}

hrv::pkc::CongruenceSystem & hrv::pkc::CongruenceSystem::Add(Congruence* congruence)
{
    _system.push_back(congruence);

    return *this;
}

bool hrv::pkc::CongruenceSystem::HasSolution() const
{
    math::MathHelper helper;
    helper.SetGCDAlgorithm(math::GCD_EUCLID_SUBTRACTION);

    for (size_t i = 1; i < _system.size(); ++i)
        if (helper.GCD(_system[i]->GetMod(), _system[i - 1]->GetMod()) != 1 ||
            _system[i - 1]->GetRHS() >= _system[i - 1]->GetMod())
            return false;

    return true && _system.size() > 0;
}

hrv::pkc::Congruence hrv::pkc::CongruenceSystem::Solve() const
{
    Congruence solution;

    if (!HasSolution())
        return solution;

    solution.SetUnknown(_system[0]->GetUnknown());
    int32_t N = 1;
    for (auto congruence : _system)
        N *= congruence->GetMod();
    solution.SetMod(N);

    math::MathHelper helper;
    int32_t rhs = 0;
    for (auto congruence : _system)
    {
        int32_t Ni = N / congruence->GetMod();
        int32_t Ki = helper.ModularMultiplicativeInverse(Ni, congruence->GetMod());
        rhs += congruence->GetRHS() * Ni * Ki;
    }
    rhs %= N;

    return solution.SetRHS(rhs);
}

std::string hrv::pkc::CongruenceSystem::ToString() const
{
    std::string res = "{\n    ";
    for (auto congruence : _system)
        res += congruence->ToString() + "\n    ";
    res = res.substr(0, res.size() - 4) + "}";

    return res;
}

hrv::pkc::CongruenceSystem::~CongruenceSystem()
{
    for (auto congruence : _system)
        utils::SafeDelete(congruence);

    _system.clear();
}

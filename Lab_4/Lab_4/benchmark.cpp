
#include "benchmark.h"
#include <vector>
#include <future>
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>
#include <logger.hpp>

#include "math_helper.h"

hrv::benchmark::Benchmark::Benchmark()
    : _sample_size(0)
    , _output_file("")
{}

hrv::benchmark::Benchmark & hrv::benchmark::Benchmark::Init(const size_t sample_size, const std::string & output_file)
{
    _sample_size = sample_size;
    _output_file = output_file;

    return *this;
}

hrv::benchmark::FactorizationBenchmark::FactorizationBenchmark()
: _samples()
, _regular_writer()
, _pollard_rho_writer()
{}

hrv::benchmark::FactorizationBenchmark & hrv::benchmark::FactorizationBenchmark::Init(const size_t sample_size, const std::string & output_file)
{
    Benchmark::Init(sample_size, output_file);

    hrv::math::MathHelper::RandGenerator rand(DIGIT_MIN, DIGIT_MAX);
    _samples.reserve(_sample_size);
    for (size_t i = 0; i < _sample_size; ++i)
    {
        uint8_t digit_count = static_cast<uint8_t>(rand.Rand());
        _samples.push_back
        (
            std::make_pair
            (
                digit_count,
                math::MathHelper::GenRandUnsignedInt(digit_count)
            )
        );
    }
    io::FileWriter::Format format = _output_file.find(".csv") != std::string::npos ? io::FileWriter::FORMAT_CSV : io::FileWriter::FORMAT_REGULAR;
    _regular_writer.Open("regular_" + output_file, format);
    _pollard_rho_writer.Open("pollard_rho_" + output_file, format);

    return *this;
}

hrv::benchmark::FactorizationBenchmark & hrv::benchmark::FactorizationBenchmark::Run()
{
    std::function<void(const size_t, const FactorizationAlgorithm)> thread_fn = [this](const size_t index, const FactorizationAlgorithm algorithm)
    {
        std::string algorithm_used = (algorithm == FACTORIZATION_REGULAR ? "Regular factorization" : "Pollard's rho factorization");

        const uint8_t& digit_count = _samples[index].first;
        const boost::multiprecision::cpp_int& val = _samples[index].second;

        _log_mutex.lock();
        LOG(INFO) << "Computing factorization of " << val << " using " << algorithm_used;
        _log_mutex.unlock();

        std::vector<boost::multiprecision::cpp_int> results;
        
        boost::timer::cpu_timer timer;
        if (algorithm == FACTORIZATION_REGULAR)
            results = hrv::math::MathHelper::FactorizeRegular(val);
        else
            results = hrv::math::MathHelper::FactorizePollardRho(val);

        timer.stop();
        auto ns = boost::chrono::nanoseconds(timer.elapsed().user + timer.elapsed().system);
        auto ms = boost::chrono::duration_cast<boost::chrono::milliseconds>(ns);
        std::stringstream ss;
        ss << val;
        if (algorithm == FACTORIZATION_REGULAR)
        {
            _regular_mutex.lock();
            _regular_writer << ss.str() << std::to_string(ms.count()) << "\r\n";
            _regular_mutex.unlock();
        }
        else
        {
            _pollard_rho_mutex.lock();
            _pollard_rho_writer << ss.str() << std::to_string(ms.count()) << "\r\n";
            _pollard_rho_mutex.unlock();
        }

        ss.str("");
        ss << "[ ";
        for (boost::multiprecision::cpp_int result : results)
            ss << result << " ";
        ss << "]";

        _log_mutex.lock();
        LOG(INFO) << "Computed factorization of " << val << " using " << algorithm_used << ". Result: " << ss.str();
        _log_mutex.unlock();
    };

    std::vector<std::future<void>> futures;
    for (size_t i = 0; i < _sample_size; ++i)
    {
        futures.push_back
        (
            std::async(thread_fn, i, FACTORIZATION_REGULAR)
        );
        futures.push_back
        (
            std::async(thread_fn, i, FACTORIZATION_POLLARD_RHO)
        );
    }

    for (size_t i = 0; i < futures.size(); ++i)
        futures[i].get();

    return *this;
}

hrv::benchmark::FactorizationBenchmark::~FactorizationBenchmark()
{
    _regular_writer.Close();
    _pollard_rho_writer.Close();
}

#define ELPP_STL_LOGGING
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP

#include "math_helper.h"
#include "benchmark.h"

using namespace hrv::benchmark;

enum RetCode
{
    SUCCESS = 0,
    FAILURE = -1
};

boost::multiprecision::cpp_int Fn(const boost::multiprecision::cpp_int& x)
{
    return boost::multiprecision::pow(x, 2) + x + 1;
}

int main()
{
    el::Loggers::configureFromGlobal("logging.conf");
    FactorizationBenchmark benchmark;
    benchmark.Init(10, "factorization.csv")
        .Run();
    return SUCCESS;
}
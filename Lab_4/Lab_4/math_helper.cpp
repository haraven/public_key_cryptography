#include "math_helper.h"
#include <boost/multiprecision/integer.hpp>
#include <boost/multiprecision/miller_rabin.hpp>

const static size_t MILLER_RABIN_TRIAL_COUNT = 25;

hrv::math::MathHelper::RandGenerator::RandGenerator(int32_t min, int32_t max, uint32_t seed)
    : _gen(seed)
    , _dist(min, max)
{}

int hrv::math::MathHelper::RandGenerator::Rand()
{
    return _dist(_gen);
}

std::vector<boost::multiprecision::cpp_int> hrv::math::MathHelper::FactorizeRegular(boost::multiprecision::cpp_int val)
{
    std::vector<boost::multiprecision::cpp_int> res;
    if (val < 2 || boost::multiprecision::miller_rabin_test(val, MILLER_RABIN_TRIAL_COUNT))
    {
        res.push_back(val);
        return res;
    }

    std::vector<boost::multiprecision::cpp_int> primes = PrimeSieve(boost::multiprecision::sqrt(val));
    for (auto prime : primes)
    {
        if (val == 0)
            return res;
        while (val % prime == 0 && val != 0)
        {
            res.push_back(prime);
            val /= prime;
        }
    }
    if (val > 1)
        res.push_back(val);

    return res;
}

std::vector<boost::multiprecision::cpp_int> hrv::math::MathHelper::FactorizePollardRho(boost::multiprecision::cpp_int val, const PollardRhoFn& f)
{
    std::vector<boost::multiprecision::cpp_int> res;
    if (val < 2 || boost::multiprecision::miller_rabin_test(val, MILLER_RABIN_TRIAL_COUNT))
    {
        res.push_back(val);

        return res;
    }

    while (val != 0)
    {
        auto divisor = PollardRhoFindFactor(val, f);
        if (divisor == -1)
        {
            res.push_back(val);
            break;
        }
        while (val % divisor == 0 && val != 0)
        {
            res.push_back(divisor);
            val /= divisor;
        }
    }

    return res;
}

boost::multiprecision::cpp_int hrv::math::MathHelper::GenRandUnsignedInt(size_t digit_count)
{
    static RandGenerator rand(0, INT_MAX);

    std::string str_val = "";
    while (digit_count)
    {
        int32_t rand_val = rand.Rand();

        while (rand_val && digit_count)
        {
            str_val += std::to_string(rand_val % 10);
            --digit_count;
            rand_val /= 10;
        }
    }
    if (str_val[0] == '0' && str_val.size() > 1)
    {
        size_t replacement = str_val.find_last_of("123456789");
        str_val.replace(str_val.begin(), str_val.begin() + 1, str_val.substr(replacement, 1));
    }

    return boost::multiprecision::cpp_int(str_val);
}

boost::multiprecision::cpp_int hrv::math::MathHelper::ImplicitFn(const boost::multiprecision::cpp_int & x)
{
    return boost::multiprecision::cpp_int(boost::multiprecision::pow(x, 2) + 1);
}

boost::multiprecision::cpp_int hrv::math::MathHelper::PollardRhoFindFactor(const boost::multiprecision::cpp_int& val, PollardRhoFn f)
{
    boost::multiprecision::cpp_int a = 2, b = 2, d = 1;
    if (a < 0)
        a = -a;
    if (b < 0)
        b = -b;
    while (d == 1)
    {
        a = Mod(f(a), val);
        b = Mod(f(b), val);
        b = Mod(f(b), val);
        d = GCDBinary(boost::multiprecision::abs(a - b), val);
    }

    if (1 < d && d < val)
        return d;

    return -1;
}

boost::multiprecision::cpp_int hrv::math::MathHelper::GCDBinary(boost::multiprecision::cpp_int a, boost::multiprecision::cpp_int b)
{
    if (a == 0) return b;
    if (b == 0) return a;

    int shift;
    for (shift = 0; ((a | b) & 1) == 0; ++shift)
    {
        a >>= 1;
        b >>= 1;
    }

    while ((a & 1) == 0)
        a >>= 1;

    do
    {
        while ((b & 1) == 0)
            b >>= 1;

        if (a > b)
            a.swap(b);
        b -= a;
    } while (b != 0);

    return a << shift;
}


boost::multiprecision::cpp_int hrv::math::MathHelper::GCDEuclidSub(boost::multiprecision::cpp_int a, boost::multiprecision::cpp_int b)
{
    if (a == 0)
        return b;
    if (b == 0)
        return a;

    if (a < 0)
        a = -a;
    if (b < 0)
        b = -b;

    while (a != b)
        if (a > b)
            a -= b;
        else
            b -= a;

    return a;
}

boost::multiprecision::cpp_int hrv::math::MathHelper::ModularMultiplicativeInverse(boost::multiprecision::cpp_int n, const boost::multiprecision::cpp_int& p)
{
    n = Mod(n, p);
    for (boost::multiprecision::cpp_int x = 1; x < p; ++x)
        if (Mod(n * x, p) == 1)
            return x;
    return 0;
}

boost::multiprecision::cpp_int hrv::math::MathHelper::Mod(const boost::multiprecision::cpp_int & val, const boost::multiprecision::cpp_int & n)
{
    boost::multiprecision::cpp_int r = val % n;
    if (((n > 0) && (r < 0)) || ((n < 0) && (r > 0)))
        r += n;
    return r;
}

std::vector<boost::multiprecision::cpp_int> hrv::math::MathHelper::PrimeSieve(const boost::multiprecision::cpp_int & max)
{
    std::vector<boost::multiprecision::cpp_int> sieve;
    std::vector<boost::multiprecision::cpp_int> primes;
    if (max == 0)
        return primes;
    for (boost::multiprecision::cpp_int i = 1; i < max + 1; ++i)
        sieve.push_back(i);
    sieve[0] = 0;
    for (boost::multiprecision::cpp_int i = 2; i < max + 1; ++i)
    {
        size_t index = i.convert_to<size_t>();
        if (sieve[index - 1] != 0)
        {
            primes.push_back(sieve[index - 1]);
            for (boost::multiprecision::cpp_int j = 2 * sieve[index - 1]; j < max + 1; j += sieve[index - 1])
                sieve[j.convert_to<size_t>() - 1] = 0;
        }
    }

    return primes;
}

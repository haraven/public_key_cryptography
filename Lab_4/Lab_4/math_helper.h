#pragma once

#include <functional>
#include <boost\multiprecision\cpp_int.hpp>
#include <boost\random.hpp>

namespace hrv
{
    namespace math
    {
        class MathHelper
        {
        public:
            class RandGenerator
            {
            public:
                RandGenerator(int32_t min, int32_t max, uint32_t seed = static_cast<const uint32_t>(time(NULL)));

                int Rand();
            private:
                boost::random::mt19937 _gen;
                boost::random::uniform_int_distribution<> _dist;
            };

            typedef std::function<boost::multiprecision::cpp_int(const boost::multiprecision::cpp_int&)> PollardRhoFn;
        private:
            static boost::multiprecision::cpp_int ImplicitFn(const boost::multiprecision::cpp_int& x);
            static boost::multiprecision::cpp_int PollardRhoFindFactor(const boost::multiprecision::cpp_int& val, PollardRhoFn f);
        public:
            static boost::multiprecision::cpp_int GCDEuclidSub(boost::multiprecision::cpp_int a, boost::multiprecision::cpp_int b);
            static boost::multiprecision::cpp_int GCDBinary(boost::multiprecision::cpp_int a, boost::multiprecision::cpp_int b);

            static boost::multiprecision::cpp_int Mod(const boost::multiprecision::cpp_int& val, const boost::multiprecision::cpp_int& n);

            static boost::multiprecision::cpp_int ModularMultiplicativeInverse(boost::multiprecision::cpp_int n, const boost::multiprecision::cpp_int& p);

            static std::vector<boost::multiprecision::cpp_int> PrimeSieve(const boost::multiprecision::cpp_int& max);
            static std::vector<boost::multiprecision::cpp_int> FactorizeRegular(boost::multiprecision::cpp_int val);
            static std::vector<boost::multiprecision::cpp_int> FactorizePollardRho(boost::multiprecision::cpp_int val, const PollardRhoFn& f = ImplicitFn);

            static boost::multiprecision::cpp_int GenRandUnsignedInt(size_t digit_count);
        };
    }
}


#pragma once
#include <string>
#include <mutex>
#include <cstdint>
#include <boost\multiprecision\cpp_int.hpp>
#include "file_writer.h"

namespace hrv
{
    namespace benchmark
    {
        class Benchmark
        {
        public:
            Benchmark();

            virtual hrv::benchmark::Benchmark& Init(const size_t sample_size, const std::string& output_file);

            virtual Benchmark& Run() = 0;

        protected:
            size_t _sample_size;
            std::string _output_file;
        };

        class FactorizationBenchmark : public Benchmark
        {
        private:
            const static size_t DIGIT_MIN = 10;
            const static size_t DIGIT_MAX = 10;
        public:
            enum FactorizationAlgorithm
            {
                FACTORIZATION_REGULAR,
                FACTORIZATION_POLLARD_RHO
            };

            FactorizationBenchmark();

            virtual hrv::benchmark::FactorizationBenchmark& Init(const size_t sample_size, const std::string& output_file) final;

            virtual hrv::benchmark::FactorizationBenchmark& Run() final;

            ~FactorizationBenchmark();
        private:
            io::FileWriter _regular_writer;
            io::FileWriter _pollard_rho_writer;
            std::vector<std::pair<uint8_t, boost::multiprecision::cpp_int>> _samples;

            std::mutex _log_mutex;
            std::mutex _regular_mutex;
            std::mutex _pollard_rho_mutex;
        };
    }
}


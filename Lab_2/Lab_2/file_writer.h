#pragma once

#include <fstream>

namespace hrv
{
    namespace io
    {
        class FileWriter
        {
        public:
            enum Format
            {
                FORMAT_REGULAR = 0x01,
                FORMAT_CSV     = 0x02
            };

            FileWriter();
            FileWriter(const std::string& filename, const Format& format = FORMAT_REGULAR);

            hrv::io::FileWriter& SetFormat(const Format& format);

            hrv::io::FileWriter& Open(const std::string& filename, const Format& format = FORMAT_REGULAR);
            hrv::io::FileWriter& operator<<(const std::string&);

            hrv::io::FileWriter& Close();

            ~FileWriter();
        private:
            std::ofstream _file;

            uint16_t _format;
        };
    }
}
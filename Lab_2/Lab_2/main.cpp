#define ELPP_STL_LOGGING
#include <logger.hpp>
INITIALIZE_EASYLOGGINGPP
//
//#include <boost\multiprecision\cpp_int.hpp>
//#include "gcd_benchmark.h"
//
//enum RetCode
//{
//    SUCCESS = 0,
//    FAILURE = -1
//};
//
//cpp_int Euler(size_t p, size_t q)
//{
//    return (p - 1) * (q - 1);
//}
//
//size_t GetKeyCodeFor(char letter)
//{
//    if (letter == ' ')
//        return 0;
//    return letter - 64;
//}
//
//size_t GetKeyCodeFor(std::string str)
//{
//    size_t res = 0;
//    size_t mypow = static_cast<size_t>(std::pow(27, str.size() - 1));
//    for (char ch : str)
//    {
//        res += mypow * GetKeyCodeFor(ch);
//        mypow /= 27;
//    }
//
//    return res;
//}
//
//size_t GetHighestPowOfNFrom(cpp_int val, size_t n)
//{
//    cpp_int myval = 1;
//    size_t mypow = 0;
//    while (myval <= val)
//    {
//        myval *= n;
//        ++mypow;
//    }
//
//    return --mypow;
//}
//
//std::vector<size_t> GetAsPowIndicesOfN(cpp_int val, size_t n)
//{
//    std::vector<size_t> res;
//
//    while (val)
//    {
//        res.push_back(GetHighestPowOfNFrom(val, n));
//        val -= static_cast<int>(std::pow(n, res[res.size() - 1]));
//    }
//
//    return res;
//}
//
//cpp_int Pow(cpp_int a, cpp_int pow, cpp_int mod)
//{
//    cpp_int res = a;
//    size_t last_pow = GetHighestPowOfNFrom(pow, 2);
//    size_t crt_pow = 0;
//    std::vector<cpp_int> results;
//    while (crt_pow <= last_pow)
//    {
//        results.push_back(res);
//        LOG(INFO) << a << "^2^" << crt_pow++ << ": " << res;
//        res *= res;
//        res %= mod;
//    }
//    
//
//    std::vector<size_t> pows = GetAsPowIndicesOfN(pow, 2);
//    LOG(INFO) << "Take powers: " << pows;
//    res = 1;
//    for (size_t mypow : pows)
//        res *= results[mypow];
//
//    res %= mod;
//
//    return res;
//}
//
//cpp_int MultiplicativeInverse(cpp_int a, cpp_int b)
//{
//    cpp_int b0 = b, t, q;
//    cpp_int x0 = 0, x1 = 1;
//    if (b == 1) return 1;
//    while (a > 1) {
//        q = a / b;
//        t = b, b = a % b, a = t;
//        t = x0, x0 = x1 - q * x0, x1 = t;
//    }
//    if (x1 < 0) x1 += b0;
//    return x1;
//}
//
//char GetCharFor(size_t code)
//{
//    if (code == 0)
//        return ' ';
//    return static_cast<char>(code) + 64;
//}
//
//std::string GetStringFor(cpp_int keycode)
//{
//    std::string tmp = "";
//    std::stringstream ss;
//    ss << keycode << " = ";
//    while (keycode)
//    {
//        size_t mypow = GetHighestPowOfNFrom(keycode, 27);
//        size_t pow27 = static_cast<size_t>(std::pow(27, mypow));
//        size_t letter = keycode.convert_to<size_t>() / pow27;
//        ss << std::to_string(letter) << " * 27^" << std::to_string(mypow) << " + ";
//        tmp += GetCharFor(letter);
//        keycode -= letter * pow27;
//    }
//
//    LOG(INFO) << ss.str();
//
//    return tmp;
//}
//
//int main()
//{
//    std::string msg = "RB";
//    size_t p = 79, q = 89;
//    cpp_int n = p * q;
//    cpp_int phi = Euler(p, q);
//    size_t e = 85;
//    hrv::math::MathHelper helper;
//    assert(helper.GCD(e, phi) == 1);
//    cpp_int d = MultiplicativeInverse(e, phi);
//
//    LOG(INFO) << "n: " << n;
//    LOG(INFO) << "phi(n): " << phi;
//    LOG(INFO) << "d: " << d;
//    LOG(INFO) << "Message: " << msg;
//    cpp_int pow1 = Pow(GetKeyCodeFor(msg), e, n);
//    LOG(INFO) << "pow1: " << pow1;
//    std::string encrypted_msg = GetStringFor(pow1);
//    LOG(INFO) << "Encrypted text: " << encrypted_msg;
//    pow1 = Pow(GetKeyCodeFor(encrypted_msg), d, n);
//    LOG(INFO) << "pow2: " << pow1;
//    LOG(INFO) << "Decrypted text: " << GetStringFor(pow1);
//    /*cpp_int n = 1643;
//    cpp_int a = 66;
//    a *= 1521 * 39;
//    LOG(INFO) << a % n;*/
//    //el::Loggers::configureFromGlobal("logging.conf");
//
//    //const size_t SAMPLE_SIZE      = 10;
//    //const std::string OUTPUT_FILE = "res.csv";
//
//    //hrv::benchmark::Benchmark* benchmark = new hrv::benchmark::GCDBenchmark();
//    //try
//    //{
//    //    benchmark->Init(SAMPLE_SIZE, OUTPUT_FILE);
//    //    benchmark->Run();
//    //    delete benchmark;
//    //}
//    //catch (std::exception& e)
//    //{
//    //    delete benchmark;
//    //    LOG(ERROR) << e.what() << std::endl;
//    //    return FAILURE;
//    //}
//
//    return SUCCESS;
//}

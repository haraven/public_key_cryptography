#include "gcd_benchmark.h"
#include <vector>
#include <thread>
#include <future>
#include <functional>
#include <algorithm>
#include <logger.hpp>
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>

using namespace hrv::math;

void hrv::benchmark::GCDBenchmark::RunSingleGCD(const size_t index, const math::GCDAlgorithm algorithm)
{}

hrv::benchmark::GCDBenchmark::GCDBenchmark()
: Benchmark()
, _division_writer()
, _samples()
, _helper()
{}

hrv::benchmark::GCDBenchmark & hrv::benchmark::GCDBenchmark::Init(const size_t sample_size, const std::string & output_file)
{
    Benchmark::Init(sample_size, output_file);

    hrv::math::RandGenerator rand(DIGIT_MIN, DIGIT_MAX);

    _samples.reserve(_sample_size);
    for (size_t i = 0; i < _sample_size; ++i)
    {
        uint8_t digit_count = static_cast<uint8_t>(rand.Rand());
        _samples.push_back
        (
            std::tuple<uint8_t, cpp_int, cpp_int>
            (
                digit_count,
                _helper.GenRandNumber(digit_count),
                _helper.GenRandNumber(digit_count)
            )
        );
    }

    io::FileWriter::Format format = _output_file.find(".csv") != std::string::npos ? io::FileWriter::FORMAT_CSV : io::FileWriter::FORMAT_REGULAR;
    _division_writer.Open("division_" + _output_file, format);
    _subtraction_writer.Open("subtraction_" + _output_file, format);
    _binary_writer.Open("binary_" + _output_file, format);

    return *this;
}

hrv::benchmark::GCDBenchmark & hrv::benchmark::GCDBenchmark::Run()
{
    std::function<void(const size_t, const math::GCDAlgorithm)> thread_fn = [this](const size_t index, const math::GCDAlgorithm algorithm)
    {
        std::string algorithm_used = (algorithm == GCD_EUCLID_DIVISION ? "Euclidean division" : algorithm == GCD_EUCLID_SUBTRACTION ? "Euclidean subtraction" : "Stein's binary algorithm");

        const uint8_t& dig_count = std::get<0>(_samples[index]);
        const cpp_int& a = std::get<1>(_samples[index]);
        const cpp_int& b = std::get<2>(_samples[index]);

        _log_mutex.lock();
        LOG(INFO) << "Computing gcd(" << a << ", " << b << "), using " << algorithm_used;
        _log_mutex.unlock();

        boost::timer::cpu_times cpu_times;
        boost::timer::cpu_timer timer;
        cpp_int res = _helper.GCD(a, b, algorithm);
        timer.stop();
        cpu_times = timer.elapsed();

        switch (algorithm)
        {
        case GCD_EUCLID_DIVISION:
            _division_writer_mutex.lock();
            _division_writer << std::to_string(dig_count) << std::to_string(cpu_times.wall) << "\r\n";
            _division_writer_mutex.unlock();
            break;
        case GCD_EUCLID_SUBTRACTION:
            _subtraction_writer_mutex.lock();
            _subtraction_writer << std::to_string(dig_count) << std::to_string(cpu_times.wall) << "\r\n";
            _subtraction_writer_mutex.unlock();
            break;
        case GCD_STEIN_BINARY:
            _binary_writer_mutex.lock();
            _binary_writer << std::to_string(dig_count) << std::to_string(cpu_times.wall) << "\r\n";
            _binary_writer_mutex.unlock();
            break;
        default:
            break;
        }

        _log_mutex.lock();
        LOG(INFO) << "Computed gcd(" << a << ", " << b << "), using " << algorithm_used << ". Result is " << res;
        _log_mutex.unlock();
    };

    std::vector<std::future<void>> results;

    for (size_t i = 0; i < _sample_size; ++i)
    {
        results.push_back
        (
            std::async(thread_fn, i, GCD_EUCLID_DIVISION)
        );
        results.push_back
        (
            std::async(thread_fn, i, GCD_EUCLID_SUBTRACTION)
        );
        results.push_back
        (
            std::async(thread_fn, i, GCD_STEIN_BINARY)
        );
    }

    for (size_t i = 0; i < results.size(); ++i)
        results[i].get();

    return *this;
}

hrv::benchmark::GCDBenchmark::~GCDBenchmark()
{
    _division_writer.Close();
}

hrv::benchmark::Benchmark::Benchmark()
: _sample_size(0)
, _output_file("")
{}

hrv::benchmark::Benchmark & hrv::benchmark::Benchmark::Init(const size_t sample_size, const std::string & output_file)
{
    _sample_size = sample_size;
    _output_file = output_file;

    return *this;
}

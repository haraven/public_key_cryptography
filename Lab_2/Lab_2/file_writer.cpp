#include "file_writer.h"
#include <string>

hrv::io::FileWriter::FileWriter()
: _format(FORMAT_REGULAR)
, _file()
{}

hrv::io::FileWriter::FileWriter(const std::string & filename, const Format & format)
: _file(filename)
, _format(format)
{}

hrv::io::FileWriter & hrv::io::FileWriter::SetFormat(const Format & format)
{
    _format = format;

    return *this;
}

hrv::io::FileWriter & hrv::io::FileWriter::Open(const std::string & filename, const Format & format)
{
    if (_file.is_open())
        _file.close();
    _file.open(filename);
    _format = format;

    return *this;
}

hrv::io::FileWriter & hrv::io::FileWriter::operator<<(const std::string & str)
{
    if (!_file.is_open())
        throw std::ifstream::failure("Cannot write \"" + str + "\" to a file, because no file is open");

    if (_format & FORMAT_CSV)
    {
        if (str == "\n" || str == "\r\n")
            _file << std::endl;
        else
            _file << str << ",";
    }
    else if (_format & FORMAT_REGULAR)
        _file << str;
    
    return *this;
}

hrv::io::FileWriter & hrv::io::FileWriter::Close()
{
    if (_file.is_open())
        _file.close();

    return *this;
}

hrv::io::FileWriter::~FileWriter()
{
    Close();
}

#include "math_helper.h"
#include <string>

hrv::math::RandGenerator::RandGenerator(int32_t min, int32_t max, uint32_t seed)
    : _gen(seed)
    , _dist(min, max)
{}

int hrv::math::RandGenerator::Rand()
{
    return _dist(_gen);
}

cpp_int hrv::math::MathHelper::GCDEuclidSub(cpp_int a, cpp_int b)
{
    while (a != b)
        if (a > b)
            a -= b;
        else
            b -= a;

    return a;
}

cpp_int hrv::math::MathHelper::GCDEuclidDiv(cpp_int a, cpp_int b)
{
    cpp_int rem;
    while (b != 0)
    {
        rem = b;
        b = a % b;
        a = rem;
    }

    return a;
}

cpp_int hrv::math::MathHelper::GCDBinary(cpp_int a, cpp_int b)
{

    if (a == 0) return b;
    if (b == 0) return a;

    int shift;
    for (shift = 0; ((a | b) & 1) == 0; ++shift) 
    {
        a >>= 1;
        b >>= 1;
    }

    while ((a & 1) == 0)
        a >>= 1;

    do 
    {
        while ((b & 1) == 0)
            b >>= 1;

        if (a > b) 
            a.swap(b);
        b -= a;
    } while (b != 0);

    return a << shift;
}

hrv::math::MathHelper::MathHelper()
: _alg(GCD_EUCLID_SUBTRACTION)
{}

hrv::math::MathHelper & hrv::math::MathHelper::SetGCDAlgorithm(const GCDAlgorithm & alg)
{
    _alg = alg;

    return *this;
}

cpp_int hrv::math::MathHelper::GenRandNumber(size_t digit_count)
{
    static RandGenerator rand(0, INT_MAX);

    std::string str_val = "";
    while (digit_count)
    {
        int32_t rand_val = rand.Rand();

        while (rand_val && digit_count) 
        {
            str_val += std::to_string(rand_val % 10);
            --digit_count;
            rand_val /= 10;
        }
    }
    if (str_val[0] == '0')
    {
        size_t replacement = str_val.find_last_of("123456789");
        str_val.replace(str_val.begin(), str_val.begin() + 1, str_val.substr(replacement, 1));
    }

    return cpp_int(str_val);
}

cpp_int hrv::math::MathHelper::GCD(cpp_int a, cpp_int b)
{
    std::function<cpp_int(cpp_int, cpp_int)> gcd_fn = _alg == GCD_EUCLID_SUBTRACTION ? &MathHelper::GCDEuclidSub : _alg == GCD_EUCLID_DIVISION ? &MathHelper::GCDEuclidDiv : &MathHelper::GCDBinary;
    
    if (a < 0)
        a = -a;
    if (b < 0)
        b = -b;

    return gcd_fn(a, b);
}

cpp_int hrv::math::MathHelper::GCD(cpp_int a, cpp_int b, const GCDAlgorithm & alg)
{
    std::function<cpp_int(cpp_int, cpp_int)> gcd_fn = alg == GCD_EUCLID_SUBTRACTION ? &MathHelper::GCDEuclidSub : _alg == GCD_EUCLID_DIVISION ? &MathHelper::GCDEuclidDiv : &MathHelper::GCDBinary;

    if (a < 0)
        a = -a;
    if (b < 0)
        b = -b;

    return gcd_fn(a, b);
}

std::tuple<int32_t, int32_t, int32_t> hrv::math::MathHelper::ExtendedEuclid(int32_t a, int32_t b)
{
    int32_t u1 = 0, u2 = 1, v1 = 1, v2 = 0, q, r, u, v;
    while (b)
    {
        q = a / b;
        r = a - q * b;
        u = u2 - q * u1;
        v = v2 - q * v1;
        a = b;
        b = r;
        u2 = u1;
        u1 = u;
        v2 = v1;
        v1 = v;
    }

    return std::tuple<int32_t, int32_t, int32_t>(u2, v2, a);
}

int32_t hrv::math::MathHelper::Mod(const int32_t& n, const int32_t& p)
{
    int32_t r = n % p;
    if (((p > 0) && (r < 0)) || ((p < 0) && (r > 0)))
        r += p;
    return r;
}

int32_t hrv::math::MathHelper::ModularMultiplicativeInverse(int32_t n, const int32_t& p)
{
    n = Mod(n, p);
    for (int32_t x = 1; x < p; ++x)
        if (Mod(n * x, p) == 1)
            return x;
    return 0;
}

hrv::math::MathHelper::~MathHelper()
{}

#pragma once

#include <string>
#include <mutex>
#include <boost\multiprecision\cpp_int.hpp>
#include "file_writer.h"
#include "math_helper.h"

namespace hrv
{
    namespace benchmark
    {
        class Benchmark
        {
        public:
            Benchmark();

            virtual hrv::benchmark::Benchmark& Init(const size_t sample_size, const std::string& output_file);

            virtual Benchmark& Run() = 0;

        protected:
            size_t _sample_size;
            std::string _output_file;
        };

        class GCDBenchmark : public Benchmark
        {
        private:
            const static size_t DIGIT_MIN = 1;
            const static size_t DIGIT_MAX = 20;

            void RunSingleGCD(const size_t index, const math::GCDAlgorithm algorithm);
        public:
            GCDBenchmark();
            
            virtual hrv::benchmark::GCDBenchmark& Init(const size_t sample_size, const std::string& output_file) final;

            virtual GCDBenchmark& Run() final;

            ~GCDBenchmark();
        private:
            io::FileWriter _division_writer;
            io::FileWriter _subtraction_writer;
            io::FileWriter _binary_writer;
            std::vector<std::tuple<uint8_t, boost::multiprecision::cpp_int, boost::multiprecision::cpp_int>> _samples;
            math::MathHelper _helper;

            std::mutex _log_mutex;
            std::mutex _division_writer_mutex;
            std::mutex _subtraction_writer_mutex;
            std::mutex _binary_writer_mutex;
        };
    }
}


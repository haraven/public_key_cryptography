#pragma once

#include <boost\multiprecision\cpp_int.hpp>
#include <boost\random.hpp>
using namespace boost::multiprecision;

namespace hrv
{
    namespace math
    {
        enum GCDAlgorithm
        {
            GCD_EUCLID_SUBTRACTION,
            GCD_EUCLID_DIVISION,
            GCD_STEIN_BINARY
        };

        class RandGenerator
        {
        public:
            RandGenerator(int32_t min, int32_t max, uint32_t seed = static_cast<const uint32_t>(time(NULL)));

            int Rand();
        private:
            boost::random::mt19937 _gen;
            boost::random::uniform_int_distribution<> _dist;
        };

        class MathHelper
        {
        private:
            static cpp_int GCDEuclidSub(cpp_int a, cpp_int b);
            static cpp_int GCDEuclidDiv(cpp_int a, cpp_int b);
            static cpp_int GCDBinary(cpp_int a, cpp_int b);
            
        public:
            MathHelper();

            hrv::math::MathHelper& SetGCDAlgorithm(const GCDAlgorithm& alg);
            cpp_int GenRandNumber(size_t digit_count);

            cpp_int GCD(cpp_int a, cpp_int b);
            cpp_int GCD(cpp_int a, cpp_int b, const GCDAlgorithm& alg);

            static std::tuple<int32_t, int32_t, int32_t> ExtendedEuclid(int32_t a, int32_t b);
           
            static int32_t hrv::math::MathHelper::Mod(const int32_t& n, const int32_t& p);

            static int32_t ModularMultiplicativeInverse(int32_t n, const int32_t& p);

            ~MathHelper();
        private:

            GCDAlgorithm _alg;
        };
    }
}

